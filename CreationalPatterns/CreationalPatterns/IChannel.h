#pragma once
#include "ISignal.h"
#include "Convert.h"
class IChannel
{
//	ADConvarter ad;
public:
	virtual void SendSignal(ISignal* &signal) = 0;
	virtual ISignal* GetSignal() = 0;
};


