#pragma once
#include "stdafx.h"
#include "DigitalChannel.h"
#include "DigitalSignal.h"

void DigitalChannel::SendSignal(ISignal* &signal)
{
	if (typeid(*signal) != typeid(DigitalSignal))
	{
		_signal = ad.Convert(signal);
	}
	else
		_signal = signal;
}

ISignal* DigitalChannel::GetSignal()
{
	return _signal;
}