#pragma once
#include "ITransmitter.h"

class DigitalTransmitter: public ITransmitter
{
	ADConvarter ad;
	IChannel* _channel;
public:
	DigitalTransmitter(IChannel* channel);
	virtual void SendSignal(ISignal* &signal);
};