#pragma once
#include "IAbstractFactory.h"
#include "SignalType.h"
#include "SFactory.h"

class Factory
{
public:
	IAbstractFactory* CreateAbstractFactory(SignalType type);
	IAbstractFactory* CreateAbstractFactory();
};