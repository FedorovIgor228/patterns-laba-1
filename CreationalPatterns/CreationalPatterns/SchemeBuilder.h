#pragma once
#include "ISchemeBuilder.h"
#include "Factory.h"
#include "Settings.h"

class SchemeBuilder : public ISchemeBuilder
{
	Factory _factory;
public:
	virtual Scheme CreateScheme();
	virtual Scheme CreateScheme(SignalType type);
};