#pragma once
#include "ISignal.h"
#include "AnalogSignal.h"
#include "DigitalSignal.h"

class IConverter
{
public:
	virtual ISignal* Convert(ISignal* &signal) = 0;
};

class ADConvarter
{
public:
	virtual ISignal* Convert(ISignal* &signal) //���������� � dig
	{
		std::cout << "Analog -> Digit signal" << std::endl;
		return new DigitalSignal(signal->GetCode());
	}
};
class DAConvarter
{
public:
	virtual ISignal* Convert(ISignal* &signal) //���������� � analog
	{
		std::cout << "Digit -> Analog signal" << std::endl;
		return new AnalogSignal(signal->GetCode());
	}
};
