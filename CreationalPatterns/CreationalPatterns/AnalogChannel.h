#pragma once
#include "IChannel.h"

class AnalogChannel: public IChannel
{
private:
	DAConvarter da;
	ISignal* _signal;
public:
	virtual void SendSignal(ISignal* &signal);
	virtual ISignal* GetSignal();
};