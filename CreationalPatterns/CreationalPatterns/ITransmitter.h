#pragma once
#include "IChannel.h"
#include "Convert.h"


class ITransmitter
{
public:
	virtual void SendSignal(ISignal* &signal) = 0;
};

