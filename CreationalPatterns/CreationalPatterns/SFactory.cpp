#include "stdafx.h"
#include "SFactory.h"
#include "DigitalAbstractFactory.h"
#include "DigitalChannel.h"
#include "DigitalTransmitter.h"
#include "DigitalReceiver.h"
#include "AnalogAbstractFactory.h"
#include "AnalogChannel.h"
#include "AnalogTransmitter.h"
#include "AnalogReceiver.h"
#include "AnalogSignal.h"
#include "DigitalSignal.h"

SFactory::SFactory(){
	if(Setting::getChan())
		_channel = new AnalogChannel();
	else
		_channel = new DigitalChannel();
}

ITransmitter* SFactory::CreateTransmitter()
{
	if (Setting::getTran())
		return new AnalogTransmitter(_channel);
	else
		return new DigitalTransmitter(_channel);
}

IReceiver * SFactory::CreateReceiver()
{
	if (Setting::getResi())
		return new AnalogReceiver(_channel);
	else
		return new DigitalReceiver(_channel);
}



