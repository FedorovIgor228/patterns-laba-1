#pragma once
#include "IReceiver.h"

class DigitalReceiver: public IReceiver
{
	ADConvarter ad;
	IChannel* _channel;
public:
	DigitalReceiver(IChannel* channel);
	virtual void PrintSignal();
};