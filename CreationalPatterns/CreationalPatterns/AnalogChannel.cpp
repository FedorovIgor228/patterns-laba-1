#pragma once
#include "stdafx.h"
#include "AnalogChannel.h"
#include "AnalogSignal.h"

void AnalogChannel::SendSignal(ISignal* &signal)
{
	if (typeid(*signal) != typeid(AnalogSignal))
	{
		_signal = da.Convert(signal);
	}
	else
		_signal = signal;
}

ISignal* AnalogChannel::GetSignal()
{
	return _signal;
}
