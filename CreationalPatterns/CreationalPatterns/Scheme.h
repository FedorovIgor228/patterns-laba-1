#pragma once
#include "ISignal.h"
#include "DigitalSignal.h"
#include "AnalogSignal.h"
#include "ITransmitter.h"
#include "IReceiver.h"
#include "IChannel.h"

class Scheme
{
private:
	ITransmitter* _transmitter;
	IReceiver* _receiver;
public:
	void Send(ISignal* &signal)
	{
		_transmitter->SendSignal(signal);
		_receiver->PrintSignal();
	}

	void SetTransmitter(ITransmitter* transmitter)
	{			
		_transmitter = transmitter;
	}

	void SetReceiver(IReceiver* receiver)
	{
		_receiver = receiver;
	}
};