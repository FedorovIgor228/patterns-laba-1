#include "stdafx.h"
#include "SchemeBuilder.h"


Scheme SchemeBuilder::CreateScheme()
{
	IAbstractFactory* abstractFactory = _factory.CreateAbstractFactory();

	Scheme scheme;
	scheme.SetTransmitter(abstractFactory->CreateTransmitter());
	scheme.SetReceiver(abstractFactory->CreateReceiver());

	return scheme;
}
