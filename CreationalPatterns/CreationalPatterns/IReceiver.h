#pragma once
#include "IChannel.h"
#include "Convert.h"
class IReceiver
{
public:
	virtual void PrintSignal() = 0;
};
