// CreationalPatterns.cpp : main project file.
#include <iostream>
#include "stdafx.h"
#include "SimpleSchemeBuilder.h"
#include "AnalogSignal.h"
#include "DigitalSignal.h"
#include "SchemeBuilder.h"
#include "Settings.h"

int main()
{
	Setting* se = Setting::getInstance();

	SimpleSchemeBuilder schemeBuilder;
	Scheme scheme = schemeBuilder.CreateScheme();
	ISignal* as = new AnalogSignal("10101");
	if(Setting::getSign)
		scheme.Send(as);
/*	else
		scheme.Send((new DigitalSignal("10101")));
	
	SimpleSchemeBuilder SschemeBuilder;
	Scheme scheme1 = SschemeBuilder.CreateScheme(Analog);
	scheme1.Send(new AnalogSignal("10101"));

	scheme1 = SschemeBuilder.CreateScheme(Digital);
	scheme1.Send(new DigitalSignal("10101"));*/
	system("pause");
    return 0;
}