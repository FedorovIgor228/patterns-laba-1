#pragma once
#include<iostream>

class Setting
{
private:
	static Setting* m_pinstance;
	static bool sig, transm, chan, resiv;
	Setting() {

		std::cout << "Enter your schem:" << std::endl;
		std::cout << "Enter your signal type: 0/1" << std::endl;
		std::cin >> sig;
		std::cout << "Enter your transmitter type: 0/1" << std::endl;
		std::cin >> transm;
		std::cout << "Enter your chanal type: 0/1" << std::endl;
		std::cin >> chan;
		std::cout << "Enter your resiver type: 0/1" << std::endl;
		std::cin >> resiv;

		std::cout << sig << " " << transm << " " << chan << " " << resiv << " " << std::endl;
	
	}
public:
	static Setting* getInstance();
	const static bool getSign() {
		return sig;
	}
	const static bool getTran(){
		return transm;
	}
	const static bool getChan(){
		return chan;
	}
	const static bool getResi(){
		return resiv;
	}
};
//*
//*/