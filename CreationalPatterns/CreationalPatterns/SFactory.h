#pragma once
#include "IAbstractFactory.h"
#include "ISignal.h"
#include "Settings.h"

class SFactory : public IAbstractFactory
{
private:
	IChannel* _channel;
public:
	SFactory();
	virtual ITransmitter* CreateTransmitter();
	virtual IReceiver* CreateReceiver();
};